'use strinct'

//importemas express
var express = require('express');
// Importemos el controlador
var countryController = require('../controllers/country');

// Instaciemos un objheto Router
var api = express.Router();

//Definimos el recurso GET con URL : /api/country/:id? ,
//api.get('/country/:id?', countryController.prueba);
api.get('/country/:id?', countryController.getCountry);
api.get('/countries/', countryController.getCountries);
api.post('/country', countryController.saveCountry);
api.put('/country/:id?', countryController.updateCountry);
api.delete('/country/:id?', countryController.deleteCountry);
//para utilizarl en otros ficheros e importar
module.exports = api;
