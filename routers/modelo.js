'use strinct'

//importemas express
var express = require('express');
// Importemos el controlador
var modeloController = require('../controllers/modelo');

// Instaciemos un objheto Router
var api = express.Router();

//Definimos el recurso GET con URL : /api/modelo/:id? ,
//api.get('/modelo/:id?', modeloController.prueba);
api.post('/modelo', modeloController.saveModelo);
//para utilizarl en otros ficheros e importar
module.exports = api;
