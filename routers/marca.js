'use strinct'

//importemas express
var express = require('express');
// Importemos el controlador
var marcaController = require('../controllers/marca');

// Instaciemos un objheto Router
var api = express.Router();

//Definimos el recurso GET con URL : /api/marca/:id? ,
//api.get('/marca/:id?', marcaController.prueba);
api.post('/marca', marcaController.saveMarca);
//para utilizarl en otros ficheros e importar
module.exports = api;
