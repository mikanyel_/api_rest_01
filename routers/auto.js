'use strinct'

//importemas express
var express = require('express');
// Importemos el controlador
var autoController = require('../controllers/auto');

// Instaciemos un objheto Router
var api = express.Router();

//Definimos el recurso GET con URL : /api/auto/:id? ,
//api.get('/auto/:id?', autoController.prueba);
api.get('/auto/:id?', autoController.getAuto);
api.get('/autos/', autoController.getAutos);
api.post('/auto', autoController.saveAuto);
api.put('/auto/:id?', autoController.updateAuto);
api.delete('/auto/:id?', autoController.deleteAuto);
//para utilizarl en otros ficheros e importar
module.exports = api;
