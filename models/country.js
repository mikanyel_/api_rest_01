'use strict'

//var database = require('../database'),
var mongoose = require('mongoose'),
Schema = mongoose.Schema;

var CountrySchema = new Schema(
  {
    nombre:{
      type: String,
      trim: true,
      default: '',
      required: 'Insert el nombre',
      index: {
        unique: false,
        dropDups: true
      }
    }
  },
  {
    timestamps: true
  }
);

var Country = mongoose.model('Country', CountrySchema);

module.exports = Country;
