'use strict'

//var database = require('../database'),
var mongoose = require('mongoose'),
Schema = mongoose.Schema;

var ModeloSchema = new Schema(
  {
    nombre:{
      type: String,
      trim: true,
      default: '',
      required: 'Insert el nombre del modelo',
      index: {
        unique: false,
        dropDups: true
      }
    },
    id_marca:{type:Schema.ObjectId, required:'Inserta una marca del modelo'}
  },
  {
    timestamps: true
  }
);

var Modelo = mongoose.model('Modelo', ModeloSchema);
module.exports = Modelo;
