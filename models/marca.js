'use strict'

//var database = require('../database'),
var mongoose = require('mongoose'),
Schema = mongoose.Schema;

//var Modelo = require('./Modelo')

var MarcaSchema = new Schema(
  {
    nombre:{
      type: String,
      trim: true,
      default: '',
      required: 'Insert el nombre de la marca',
      index: {
        unique: true,
        dropDups: true
      }
    },
    pais:{
      type: String,
      trim: true,
      default: '',
      required: 'Insert el pais',
      index: {
        unique: false,
        dropDups: true
      }
    },
    fechaCreacion:{
      type: Date,
      trim: true,
      default: '',
      required: 'Insert una fecha',
      index: {
        unique: false,
        dropDups: true
      }
    }
    // ,
    // modelos: [Modelo.schema]
  },
  {
    timestamps: true
  }
);

var Marca = mongoose.model('Marca', MarcaSchema);
module.exports = Marca;
