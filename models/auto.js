'use strict'

//var database = require('../database'),
var mongoose = require('mongoose'),
Schema = mongoose.Schema;

var AutoSchema = new Schema(
  {
    marca:{
      type: String,
      trim: true,
      default: '',
      required: 'Insert una marca',
      index: {
        unique: false,
        dropDups: true
      }
    },
    modelo:{
      type: String,
      default: '',
      required: 'Insert una modelo',
      index: {
        unique: false,
        dropDups: true
      }
    },
    anio:{
      type: Number,
      required: 'Insert un año',
      index: {
        unique: false,
        dropDups: true
      }
    },
    version:{
      type: String,
      trim: true,
      default: '',
      required: 'Insert una version',
      index: {
        unique: false,
        dropDups: true
      }
    },
    colores:[String],
    serialVersion: {
      type: String,
      trim: true,
      required: 'Insert el serial version',
      index: {
        unique: true,
        dropDups: true
      }
    },
    motorInfo: {
      transmision:{
        type: String,
        required: 'Insert la trasmision',
        index: {
          unique: false,
          dropDups: true
        },
        enum: [
          'manual',
          'automatico'
        ]
      }
    }
  },
  {
    timestamps: true
  }
);

var Auto = mongoose.model('Auto', AutoSchema);

module.exports = Auto;
