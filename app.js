//importamos
var express = require('express');
var bodyParser = require('body-parser');

// declaremos la variable app como instaciia de express
var app = express();
var auto = require('./routers/auto');
var country = require('./routers/country');
var marca = require('./routers/marca');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));

app.use(function (req, res, next){
  // puede ser cosumida desde cualquier lugar
  res.header('Access-Control-Allow-Origin','*');
  // Cabeceras permitidas
  res.header('Access-Control-Allow-Headers','X-API-KEY,Origin,X-Requested-With,Content-Type, Accept, Access-Control-Request-Method');
  // metodos permitdos
  res.header('Access-Control_Allow-Methods', 'GET','POST','PUT','DELETE');
  res.header('Allow', 'GET','POST','PUT','DELETE');
  next();
})
// url de la API
app.use('/api', auto),
app.use('/api', country)
app.use('/api', marca)


// para utilizarlo en otros ficheros e importar
module.exports = app;
