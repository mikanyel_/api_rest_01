'use strict'

var mongoose = require('mongoose');
var Marca = require('../models/marca');

function saveMarca(req, res){
  var marca = new Marca(req.body);

  var parts = req.body.fechaCreacion.split('-');
  marca.fechaCreacion = new Date (parts[0], parts[1]-1, parts[2]);

  marca.save(function(err, data){
    if (err) {
      console.log(err);
      res.status(500).send({message: 'Error al guardar la marca', error: err});
    } else {
      res.status(200).send({saved: data})
    }
  });
};

module.exports = {
  saveMarca
}
