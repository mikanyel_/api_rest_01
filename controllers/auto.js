'use strict'

var Auto = require('../models/auto');
var mongoose = require('mongoose');

function prueba(req, res){
  if (req.params.id){
    var id = req.params.id;
  } else {
    var id = "Sin ID"
  }
  res.status(200).send(
    {
      message:"Este es el ID: "+ id
    }
  )
}

function getAuto(req, res){
  // var autoId = req.params.id;
  // res.status(200).send({ data: "autoId"})
  var autoId = req.params.id;
  var idValido = mongoose.Types.ObjectId.isValid(autoId);

  if(!idValido){
    res.status(409).send({message: 'ID invalido'});
  } else {
    Auto.findById(autoId, function(err, auto){
      if(err){
        console.log(err);
        res.status(500).send({ message: 'Error al obtener el auto', error: err});
      }
      else {
        if(!auto){
          res.status(404).send({message: 'No existe el Auto con el ID propersionado'});
        } else {
          res.status(200).send({auto})
        }
      }
    });

  }
}

function getAutos(req, res){
  // var params = req.body;
  // res.status(200).send({ metodo: "getAutos"})

  Auto.find({}).sort('anio').exec(function (err, autos){
    if(err){
      console.log(err);
      res.status(500).send({message: 'Error al obtener los datos', error : err});
    } else {
      res.status(200).send({autos})
    }
  });
}

function saveAuto(req, res){
  var auto = new Auto(req.body);

  auto.save(function(err, data){
    if (err) {
      console.log(err);
      res.status(500).send({message: 'Error al guardar el auro', error: err});
    } else {
      res.status(200).send({data: data})
    }
  });
};

function updateAuto(req, res){
  // var params = req.body;
  // res.status(200).send({ metodo: "updateAuto", auto: params})
  var autoId = req.params.id;
  var idValido = mongoose.Types.ObjectId.isValid(autoId);

  if(!idValido){
    res.status(409).send({message: 'ID invalido'});
  } else {
    /*Auto.findByIdAndUpdate(autoId, req.body, function(err, autoUpdate){
      if(err){
        console.log(err);
        res.status(500).send({ message: 'Error al actualizar el auto', error: err});
      }
      else {
        if(!autoUpdate){
          res.status(404).send({ message: 'No existe el auto con el ID proporsionado'});
        }
        else {
          Auto.findById(autoId, function (err, autoNuevo){
            res.status(200).send({ viejo:autoUpdate, nuevo: autoNuevo});
          });
       }
     }
   });*/
   Auto.findByIdAndUpdate(autoId, req.body, {new:true}, function(err, autoUpdate){
     if(err){
       console.log(err);
       res.status(500).send({ message: 'Error al actualizar el auto', error: err});
     }
     else {
       if(!autoUpdate){
         res.status(404).send({ message: 'No existe el auto con el ID proporsionado'});
       }
       else {
         res.status(200).send({ data: autoUpdate});
       }
     }
  });
 }
}

function deleteAuto(req, res){
  // var params = req.body;
  // res.status(200).send({ metodo: "deleteAuto", auto: params})
  var autoId = req.params.id;
  var idValido = mongoose.Types.ObjectId.isValid(autoId);

  if(!idValido){
    res.status(409).send({message: 'ID invalido'});
  } else {
    Auto.findById(autoId, function(err, auto){
      if(err){
        console.log(err);
        res.status(500).send({ message: 'Error al actualizar el auto', error: err});
      }
      else {
        if(!auto){
          res.status(404).send({ message: 'No existe el auto con el ID proporsionado'});
        }
        else {
          // Auto.remove(function (err){
          //   if(err){
          //     res.status(500).send({ message: 'Error al eliminar el auto', error:err});
          //   }
          //   else{
          //     res.status(200).send({ message: 'El auto se ha eliminado'});
          //   }
          // });

          Auto.findByIdAndRemove(autoId, function (err){
            if(err){
              res.status(500).send({ message: 'Error al eliminar el auto', error:err});
            }
            else{
              res.status(200).send({ message: 'El auto se ha eliminado segundo metodo'});
            }
          });

       }
     }
   });
}
}


module.exports = {
  prueba,
  getAuto,
  getAutos,
  saveAuto,
  updateAuto,
  deleteAuto
}
