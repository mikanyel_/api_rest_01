'use strict'

var Country = require('../models/country');
var mongoose = require('mongoose');

function prueba(req, res){
  if (req.params.id){
    var id = req.params.id;
  } else {
    var id = "Sin ID"
  }
  res.status(200).send(
    {
      message:"Este es el ID: "+ id
    }
  )
}

function getCountry(req, res){
  // var countryId = req.params.id;
  // res.status(200).send({ data: "countryId"})
  var countryId = req.params.id;
  var idValido = mongoose.Types.ObjectId.isValid(countryId);

  if(!idValido){
    res.status(409).send({message: 'ID invalido'});
  } else {
    Country.findById(countryId, function(err, country){
      if(err){
        console.log(err);
        res.status(500).send({ message: 'Error al obtener el country', error: err});
      }
      else {
        if(!country){
          res.status(404).send({message: 'No existe el Country con el ID propersionado'});
        } else {
          res.status(200).send({country})
        }
      }
    });

  }
}

function getCountries(req, res){
  // var params = req.body;
  // res.status(200).send({ metodo: "getCountrys"})

  Country.find({}).sort('nombre').exec(function (err, countrys){
    if(err){
      console.log(err);
      res.status(500).send({message: 'Error al obtener los datos', error : err});
    } else {
      res.status(200).send({countrys})
    }
  });
}

function saveCountry(req, res){
  var country = new Country(req.body);

  country.save(function(err, data){
    if (err) {
      console.log(err);
      res.status(500).send({message: 'Error al guardar el coutry', error: err});
    } else {
      res.status(200).send({data: data})
    }
  });
};

function updateCountry(req, res){
  // var params = req.body;
  // res.status(200).send({ metodo: "updateCountry", country: params})
  var countryId = req.params.id;
  var idValido = mongoose.Types.ObjectId.isValid(countryId);

  if(!idValido){
    res.status(409).send({message: 'ID invalido'});
  } else {
    /*Country.findByIdAndUpdate(countryId, req.body, function(err, countryUpdate){
      if(err){
        console.log(err);
        res.status(500).send({ message: 'Error al actualizar el country', error: err});
      }
      else {
        if(!countryUpdate){
          res.status(404).send({ message: 'No existe el country con el ID proporsionado'});
        }
        else {
          Country.findById(countryId, function (err, countryNuevo){
            res.status(200).send({ viejo:countryUpdate, nuevo: countryNuevo});
          });
       }
     }
   });*/
   Country.findByIdAndUpdate(countryId, req.body, {new:true}, function(err, countryUpdate){
     if(err){
       console.log(err);
       res.status(500).send({ message: 'Error al actualizar el country', error: err});
     }
     else {
       if(!countryUpdate){
         res.status(404).send({ message: 'No existe el country con el ID proporsionado'});
       }
       else {
         res.status(200).send({ data: countryUpdate});
       }
     }
  });
 }
}

function deleteCountry(req, res){
  // var params = req.body;
  // res.status(200).send({ metodo: "deleteCountry", country: params})
  var countryId = req.params.id;
  var idValido = mongoose.Types.ObjectId.isValid(countryId);

  if(!idValido){
    res.status(409).send({message: 'ID invalido'});
  } else {
    Country.findById(countryId, function(err, country){
      if(err){
        console.log(err);
        res.status(500).send({ message: 'Error al actualizar el country', error: err});
      }
      else {
        if(!country){
          res.status(404).send({ message: 'No existe el country con el ID proporsionado'});
        }
        else {
          Country.remove(countryId, function (err){
            if(err){
              res.status(500).send({ message: 'Error al eliminar el country', error:err});
            }
            else{
              res.status(200).send({ message: 'El country se ha eliminado'});
            }
          });

          // Country.findByIdAndRemove(countryId, function (err){
          //   if(err){
          //     res.status(500).send({ message: 'Error al eliminar el country', error:err});
          //   }
          //   else{
          //     res.status(200).send({ message: 'El country se ha eliminado segundo metodo'});
          //   }
          // });

       }
     }
   });
}
}


module.exports = {
  prueba,
  getCountry,
  getCountries,
  saveCountry,
  updateCountry,
  deleteCountry
}
