'use strict'

var mongoose = require('mongoose');
var Modelo = require('../models/modelo');

function saveModelo(req, res){
  var modelo = new Modelo(req.body);

  // var parts = req.body.fechaCreacion.split('-');
  // modelo.fechaCreacion = new Date (parts[0], parts[1]-1, parts[2]);

  modelo.save(function(err, data){
    if (err) {
      console.log(err);
      res.status(500).send({message: 'Error al guardar la modelo', error: err});
    } else {
      res.status(200).send({saved: data})
    }
  });
};

module.exports = {
  saveModelo
}
